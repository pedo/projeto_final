from tkinter import *
from tkinter import ttk
import pandas as pd
import csv


root = Tk()
var_cat = IntVar()
despesas = pd.read_csv('despesas.csv')
bd_despesas = pd.DataFrame(despesas)
bd_despesas['Month'] = pd.to_datetime(bd_despesas['Data'], format='%d/%m/%Y').dt.month
sorted_despesas = bd_despesas.sort_values(by=['Month', 'Data'])

def atualizar_variaveis():
    global sorted_despesas
    despesas = pd.read_csv('despesas.csv')
    bd_despesas = pd.DataFrame(despesas)
    bd_despesas['Month'] = pd.to_datetime(bd_despesas['Data'], format='%d/%m/%Y').dt.month
    sorted_despesas = bd_despesas.sort_values(by=['Month', 'Data'])


class Funcs():
    
    
    def limpa_escrita(self):
        self.add_date.delete(0, END)
        self.add_cat.delete(0, END)
        self.add_value.delete(0, END)

    #MOSTRAR NOVA DESPESA
    #CHECKBUTTONS:      ORGANIZAR POR CATEGORIA         VER TOTAL
    #EXCLUIR ITEM

    def clean_filter(self):
        self.choose_cat.set('Categorias')
        self.choose_month.set('Mês')
        self.mostrar_lista()
    def mostrar_lista(self):
        data = sorted_despesas[['Data', 'Categoria', 'Valor']]
        self.lista_despesa.delete(*self.lista_despesa.get_children())
        for row in data.values:
            row_values = [str(value) for value in row]
            self.lista_despesa.insert("", END, values=row_values)
    def filtered(self):
        selected_category = self.choose_cat.get()
        selected_date = self.choose_month.get()
        data = sorted_despesas[['Data', 'Categoria', 'Valor', 'Month']]
        if selected_category or selected_date:
            if selected_category != 'Categorias':
                data = data[data['Categoria'] == selected_category]
            if selected_date != 'Mês':
                data = data[data['Month'] == int(selected_date)]
            self.lista_despesa.delete(*self.lista_despesa.get_children())
            for row in data.values:
                row_values = [str(value) for value in row]
                self.lista_despesa.insert("", END, values=row_values)
        else:
            self.mostrar_lista()

    def nova_despesa(self):
        nova_linha = [self.add_date.get(),self.add_cat.get(),self.add_value.get()]
        with open('despesas.csv', mode='a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(nova_linha)
        self.limpa_escrita()
        atualizar_variaveis()
        self.mostrar_lista()
        
    def cat_org(self):
        global sorted_despesas
        global var_cat
        
        if var_cat.get() == 1:
            sorted_despesas = bd_despesas.sort_values(by=['Month', 'Categoria', 'Data'])
        else:
            sorted_despesas = bd_despesas.sort_values(by=['Month', 'Data'])
            atualizar_variaveis()
            
 


class App(Funcs):
    def __init__(self):
        self.root = root
        self.screen()
        self.screen_frames()
        self.widgets_func()
        self.lista_frame_2()
        self.filtered()
        self.cat_org()
        self.mostrar_lista()
        root.mainloop()

    def screen(self):
        self.root.title("Despesas")
        self.root.configure(background = '#7d0c87')
        self.root.geometry('800x600')
        self.root.resizable(True, True)
        

    def screen_frames(self):
        self.frame_1 = Frame(self.root, bd = 4, bg = '#dfe3ee', highlightbackground = '#759fe6', highlightthickness = 4)
        self.frame_1.place(relx=0.02, rely = 0.02, relwidth=0.96, relheight=0.2)
        self.frame_2 = Frame(self.root, bd = 4, bg = '#dfe3ee', highlightbackground = '#759fe6', highlightthickness = 4)
        self.frame_2.place(relx=0.02, rely = 0.26, relwidth=0.90, relheight=0.5)
        self.frame_3 = Frame(self.root, bd = 4, bg = '#dfe3ee', highlightbackground = '#759fe6', highlightthickness = 4)        
        self.frame_3.place(relx = 0.02, rely = 0.80, relwidth=0.96, relheight=0.18)

    def widgets_func(self):
            #apenas total - checkbutton
        self.only_total = Checkbutton(self.frame_1, text="Apenas total", bg = '#dfe3ee')
        self.only_total.place(relx = 0.8, rely = 0.1, relwidth = 0.2, relheight = 0.2)
            #mes - combobox
        self.choose_month = ttk.Combobox(self.frame_1, values=sorted_despesas['Month'].unique().tolist())
        self.choose_month.set("Mês")
        self.choose_month.config(state='readonly')
        self.choose_month.place(relx=0.02, rely=0.7, relwidth=0.15, relheight=0.2)
            #categoria - combobox
        self.choose_cat = ttk.Combobox(self.frame_1, values = sorted_despesas['Categoria'].unique().tolist())
        self.choose_cat.set("Categorias")
        self.choose_cat.config(state='readonly')
        self.choose_cat.place(relx=0.18, rely=0.7, relwidth=0.2, relheight=0.2)
            #ver gráfico - button
        self.see_graphics = Button(self.frame_1, text = "Ver Gráficos")
        self.see_graphics.place(relx= 0.8, rely = 0.7, relwidth=0.15, relheight=0.2)
            #filtrar - button
        self.filter = Button(self.frame_1, text = "Filtrar", command = self.filtered)
        self.filter.place(relx= 0.4, rely = 0.7, relwidth=0.15, relheight=0.2)
            #limpar filtro
        self.filter = Button(self.frame_1, text = "Limpar filtro", command = self.clean_filter)
        self.filter.place(relx= 0.55, rely = 0.7, relwidth=0.15, relheight=0.2)

        #frame 3
            #organizar por categoria - checkbutton
        self.category_org = Checkbutton(self.frame_3, text="Organizar por categoria", bg = '#dfe3ee', variable=var_cat,onvalue = 1, offvalue = 0, command = self.cat_org)
        self.category_org.place(relx = 0.8, rely = 0.1, relwidth = 0.2, relheight = 0.2)
            #DD/MM/YY - entry
        self.date_label = Label(self.frame_3, text = "Data: (D/M/A)", bg = '#dfe3ee')
        self.date_label.place(relx = 0.05, rely = 0.3)
        self.add_date = Entry(self.frame_3)
        self.add_date.place(relx=0.05, rely=0.5, relheight=0.25 )
            #category - entry
        self.cat_label = Label(self.frame_3, text = "Categoria:", bg = '#dfe3ee')
        self.cat_label.place(relx = 0.23, rely = 0.3)
        self.add_cat = Entry(self.frame_3)
        self.add_cat.place(relx=0.23, rely=0.5, relheight=0.25, relwidth=0.20 )
            #valor - entry
        self.value_lb = Label (self.frame_3, text = "Valor:", bg = '#dfe3ee')
        self.value_lb.place(relx = 0.44, rely = 0.3)
        self.add_value = Entry(self.frame_3)
        self.add_value.place(relx=0.44, rely=0.5, relheight=0.25 )
            #adicionar despesa - button
        self.add_despesa = Button(self.frame_3, text = "Adicionar despesa", command = self.nova_despesa)
        self.add_despesa.place(relx = 0.63, rely = 0.5)
            #limpar tudo
        self.apagar_tudo = Button(self.frame_3, text = 'Limpar', bg = '#dfe3ee', command = self.limpa_escrita)
        self.apagar_tudo.place(relx= 0.8, rely = 0.5)

    def lista_frame_2(self):
        
        self.lista_despesa = ttk.Treeview(self.frame_2, height=3, column=('col1', 'col2', 'col3'))
        self.lista_despesa.heading("#0", text = "")
        self.lista_despesa.heading("#1", text = "Data")
        self.lista_despesa.heading("#2", text = "Categoria")
        self.lista_despesa.heading("#3", text = "Valor")

        self.lista_despesa.column("#0", width = 1)
        self.lista_despesa.column("#1", width = 100)
        self.lista_despesa.column("#2", width = 300)
        self.lista_despesa.column("#3", width =175)

        self.lista_despesa.place(relx=0.001, rely=0.001, relwidth=0.96, relheight=1)

        self.scroll_lista = Scrollbar(self.frame_2, orient='vertical')
        self.lista_despesa.configure(yscroll = self.scroll_lista.set)
        self.scroll_lista.place(relx=0.96, rely =0.005, relwidth=0.04, relheight = 0.99)






App()
